﻿namespace AuthenticationSystem
{
    partial class authentication_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.login = new System.Windows.Forms.Button();
            this.username = new System.Windows.Forms.TextBox();
            this.password = new System.Windows.Forms.TextBox();
            this.login_panel = new System.Windows.Forms.Panel();
            this.click_to_register = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.no_a_member_label = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.register_panel = new System.Windows.Forms.Panel();
            this.cancel = new System.Windows.Forms.Button();
            this.register = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.password2_label = new System.Windows.Forms.Label();
            this.password_label = new System.Windows.Forms.Label();
            this.username_label = new System.Windows.Forms.Label();
            this.last_name_label = new System.Windows.Forms.Label();
            this.first_name_label = new System.Windows.Forms.Label();
            this.password2_r = new System.Windows.Forms.TextBox();
            this.password_r = new System.Windows.Forms.TextBox();
            this.username_r = new System.Windows.Forms.TextBox();
            this.last_name = new System.Windows.Forms.TextBox();
            this.first_name = new System.Windows.Forms.TextBox();
            this.login_panel.SuspendLayout();
            this.register_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // login
            // 
            this.login.Location = new System.Drawing.Point(106, 162);
            this.login.Name = "login";
            this.login.Size = new System.Drawing.Size(75, 23);
            this.login.TabIndex = 0;
            this.login.Text = "Login";
            this.login.UseVisualStyleBackColor = true;
            this.login.Click += new System.EventHandler(this.login_Click);
            // 
            // username
            // 
            this.username.Location = new System.Drawing.Point(106, 72);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(173, 20);
            this.username.TabIndex = 1;
            // 
            // password
            // 
            this.password.Location = new System.Drawing.Point(106, 120);
            this.password.Name = "password";
            this.password.PasswordChar = '*';
            this.password.Size = new System.Drawing.Size(173, 20);
            this.password.TabIndex = 2;
            // 
            // login_panel
            // 
            this.login_panel.Controls.Add(this.click_to_register);
            this.login_panel.Controls.Add(this.label3);
            this.login_panel.Controls.Add(this.no_a_member_label);
            this.login_panel.Controls.Add(this.label2);
            this.login_panel.Controls.Add(this.label1);
            this.login_panel.Controls.Add(this.username);
            this.login_panel.Controls.Add(this.login);
            this.login_panel.Controls.Add(this.password);
            this.login_panel.Location = new System.Drawing.Point(12, 12);
            this.login_panel.Name = "login_panel";
            this.login_panel.Size = new System.Drawing.Size(314, 259);
            this.login_panel.TabIndex = 3;
            // 
            // click_to_register
            // 
            this.click_to_register.Location = new System.Drawing.Point(128, 213);
            this.click_to_register.Name = "click_to_register";
            this.click_to_register.Size = new System.Drawing.Size(151, 23);
            this.click_to_register.TabIndex = 1;
            this.click_to_register.Text = "Click To Register";
            this.click_to_register.UseVisualStyleBackColor = true;
            this.click_to_register.Click += new System.EventHandler(this.click_to_register_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Password:";
            // 
            // no_a_member_label
            // 
            this.no_a_member_label.AutoSize = true;
            this.no_a_member_label.Location = new System.Drawing.Point(25, 218);
            this.no_a_member_label.Name = "no_a_member_label";
            this.no_a_member_label.Size = new System.Drawing.Size(82, 13);
            this.no_a_member_label.TabIndex = 0;
            this.no_a_member_label.Text = "Not a member ?";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Username:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Location = new System.Drawing.Point(22, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(273, 32);
            this.label1.TabIndex = 3;
            this.label1.Text = "Login to view users";
            // 
            // register_panel
            // 
            this.register_panel.Controls.Add(this.cancel);
            this.register_panel.Controls.Add(this.register);
            this.register_panel.Controls.Add(this.label9);
            this.register_panel.Controls.Add(this.password2_label);
            this.register_panel.Controls.Add(this.password_label);
            this.register_panel.Controls.Add(this.username_label);
            this.register_panel.Controls.Add(this.last_name_label);
            this.register_panel.Controls.Add(this.first_name_label);
            this.register_panel.Controls.Add(this.password2_r);
            this.register_panel.Controls.Add(this.password_r);
            this.register_panel.Controls.Add(this.username_r);
            this.register_panel.Controls.Add(this.last_name);
            this.register_panel.Controls.Add(this.first_name);
            this.register_panel.Location = new System.Drawing.Point(332, 12);
            this.register_panel.Name = "register_panel";
            this.register_panel.Size = new System.Drawing.Size(405, 259);
            this.register_panel.TabIndex = 5;
            // 
            // cancel
            // 
            this.cancel.Location = new System.Drawing.Point(183, 213);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(89, 23);
            this.cancel.TabIndex = 11;
            this.cancel.Text = "Cancel";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // register
            // 
            this.register.Location = new System.Drawing.Point(81, 213);
            this.register.Name = "register";
            this.register.Size = new System.Drawing.Size(87, 23);
            this.register.TabIndex = 6;
            this.register.Text = "Register";
            this.register.UseVisualStyleBackColor = true;
            this.register.Click += new System.EventHandler(this.register_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label9.Location = new System.Drawing.Point(26, 21);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(360, 32);
            this.label9.TabIndex = 10;
            this.label9.Text = "Member Regstration Form";
            // 
            // password2_label
            // 
            this.password2_label.AutoSize = true;
            this.password2_label.Location = new System.Drawing.Point(29, 179);
            this.password2_label.Name = "password2_label";
            this.password2_label.Size = new System.Drawing.Size(94, 13);
            this.password2_label.TabIndex = 9;
            this.password2_label.Text = "Repeat Password:";
            // 
            // password_label
            // 
            this.password_label.AutoSize = true;
            this.password_label.Location = new System.Drawing.Point(29, 153);
            this.password_label.Name = "password_label";
            this.password_label.Size = new System.Drawing.Size(56, 13);
            this.password_label.TabIndex = 8;
            this.password_label.Text = "Password:";
            // 
            // username_label
            // 
            this.username_label.AutoSize = true;
            this.username_label.Location = new System.Drawing.Point(29, 127);
            this.username_label.Name = "username_label";
            this.username_label.Size = new System.Drawing.Size(58, 13);
            this.username_label.TabIndex = 7;
            this.username_label.Text = "Username:";
            // 
            // last_name_label
            // 
            this.last_name_label.AutoSize = true;
            this.last_name_label.Location = new System.Drawing.Point(29, 101);
            this.last_name_label.Name = "last_name_label";
            this.last_name_label.Size = new System.Drawing.Size(61, 13);
            this.last_name_label.TabIndex = 6;
            this.last_name_label.Text = "Last Name:";
            // 
            // first_name_label
            // 
            this.first_name_label.AutoSize = true;
            this.first_name_label.Location = new System.Drawing.Point(29, 75);
            this.first_name_label.Name = "first_name_label";
            this.first_name_label.Size = new System.Drawing.Size(60, 13);
            this.first_name_label.TabIndex = 5;
            this.first_name_label.Text = "First Name:";
            // 
            // password2_r
            // 
            this.password2_r.Location = new System.Drawing.Point(172, 176);
            this.password2_r.Name = "password2_r";
            this.password2_r.PasswordChar = '*';
            this.password2_r.Size = new System.Drawing.Size(201, 20);
            this.password2_r.TabIndex = 4;
            // 
            // password_r
            // 
            this.password_r.Location = new System.Drawing.Point(172, 150);
            this.password_r.Name = "password_r";
            this.password_r.PasswordChar = '*';
            this.password_r.Size = new System.Drawing.Size(201, 20);
            this.password_r.TabIndex = 3;
            // 
            // username_r
            // 
            this.username_r.Location = new System.Drawing.Point(172, 124);
            this.username_r.Name = "username_r";
            this.username_r.Size = new System.Drawing.Size(201, 20);
            this.username_r.TabIndex = 2;
            // 
            // last_name
            // 
            this.last_name.Location = new System.Drawing.Point(172, 98);
            this.last_name.Name = "last_name";
            this.last_name.Size = new System.Drawing.Size(201, 20);
            this.last_name.TabIndex = 1;
            // 
            // first_name
            // 
            this.first_name.Location = new System.Drawing.Point(172, 72);
            this.first_name.Name = "first_name";
            this.first_name.Size = new System.Drawing.Size(201, 20);
            this.first_name.TabIndex = 0;
            // 
            // authentication_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(749, 286);
            this.Controls.Add(this.register_panel);
            this.Controls.Add(this.login_panel);
            this.Name = "authentication_form";
            this.Text = "Authentication System";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.authentication_form_closed);
            this.login_panel.ResumeLayout(false);
            this.login_panel.PerformLayout();
            this.register_panel.ResumeLayout(false);
            this.register_panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button login;
        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.Panel login_panel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel register_panel;
        private System.Windows.Forms.Button click_to_register;
        private System.Windows.Forms.Label no_a_member_label;
        private System.Windows.Forms.Label password2_label;
        private System.Windows.Forms.Label password_label;
        private System.Windows.Forms.Label username_label;
        private System.Windows.Forms.Label last_name_label;
        private System.Windows.Forms.Label first_name_label;
        private System.Windows.Forms.TextBox password2_r;
        private System.Windows.Forms.TextBox password_r;
        private System.Windows.Forms.TextBox username_r;
        private System.Windows.Forms.TextBox last_name;
        private System.Windows.Forms.TextBox first_name;
        private System.Windows.Forms.Button register;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button cancel;
    }
}

