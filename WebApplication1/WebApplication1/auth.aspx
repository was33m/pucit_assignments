﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="auth.aspx.cs" Inherits="WebApplication1.auth" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <title>ASP.Net web authentication</title>

    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="Content/styles.css?12352" rel="stylesheet" />
    
    <script src="Scripts/jquery-1.9.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/javascript.js"></script>

</head>
<body>

    <header>

        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">ASP.Net Wed Auth</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><asp:HyperLink ID="LoginLinkTop" runat="server" NavigateUrl="~/auth.aspx">Login</asp:HyperLink></li>
                        <li><asp:HyperLink ID="RegisterLinkTop" runat="server" NavigateUrl="~/register.aspx">Register</asp:HyperLink></li>
                    </ul>
                </div>
            </div>
        </nav>

    </header>

    <section class="body">
        
        <form id="form1" runat="server" class="Form LoginForm">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Login To Account</h3>
                </div>
                <div class="panel-body">

                    <div class="alert alert-danger alert-dismissible" role="alert" id="ServerSideErrorsContainer">

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>

                        <asp:Label ID="ServerSideErrors" runat="server" Text=""></asp:Label>

                    </div>

                    <div class="form-group row">

                        <label for="username" class="col-xs-4">Username</label>
                        <div class="col-xs-8">
                            <asp:TextBox ID="username" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>

                    </div>

                    <div class="form-group row">

                        <label for="userpassword" class="col-xs-4">Password</label>
                        <div class="col-xs-8">
                            <asp:TextBox ID="userpassword" runat="server" TextMode="Password" CssClass="form-control col-xs-10"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group row">

                        <div class="col-xs-4">
                            <asp:Button ID="login" runat="server" Text="Login" OnClick="login_Click" CssClass="btn btn-primary" />
                        </div>

                    </div>

                    <div class="form-group row">

                        <div class="col-xs-12">
                            Not a member ? <asp:HyperLink ID="RegisterLink" runat="server" NavigateUrl="~/register.aspx">Click Here</asp:HyperLink> to become a member.
                        </div>

                    </div>

                </div>
            </div>


        </form>

    </section>

    <footer></footer>

    


</body>
</html>
