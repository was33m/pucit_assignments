﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="search_data.aspx.cs" Inherits="WebApplication1.search_data" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <title>Search User Data</title>
    <link rel="stylesheet" href="resources/styles.css" />
    <script type="text/javascript" src="resources/jquery-3.1.0.min.js"></script>
    <script type="text/javascript" src="resources/javascript.js"></script>

</head>
<body>
    <form id="form1" runat="server" class="Form SearchForm" >

        <div class="row">
            <div class="column">

                Search text

            </div>
            <div class="column">

                <asp:TextBox ID="searchinput1" runat="server"></asp:TextBox>

            </div>
            <div style="clear:both"></div>
            
        </div>

        <div class="row">
            <asp:Button ID="search_button" runat="server" Text="Search Records" OnClick="search_button_Click" />
        </div>


        <div class="row">

            <div class="column">

                <asp:GridView ID="GridView1" runat="server">
                </asp:GridView>

            </div>
        </div>


    </form>
</body>
</html>
