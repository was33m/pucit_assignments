﻿namespace AuthenticationSystem
{
    partial class users_grid
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.users_grid_view = new System.Windows.Forms.DataGridView();
            this.close = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.users_grid_view)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(206, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "User List";
            // 
            // users_grid_view
            // 
            this.users_grid_view.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.users_grid_view.Location = new System.Drawing.Point(12, 44);
            this.users_grid_view.Name = "users_grid_view";
            this.users_grid_view.Size = new System.Drawing.Size(548, 272);
            this.users_grid_view.TabIndex = 1;
            // 
            // close
            // 
            this.close.Location = new System.Drawing.Point(12, 335);
            this.close.Name = "close";
            this.close.Size = new System.Drawing.Size(548, 23);
            this.close.TabIndex = 2;
            this.close.Text = "Close";
            this.close.UseVisualStyleBackColor = true;
            this.close.Click += new System.EventHandler(this.close_Click);
            // 
            // users_grid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(572, 383);
            this.Controls.Add(this.close);
            this.Controls.Add(this.users_grid_view);
            this.Controls.Add(this.label1);
            this.Name = "users_grid";
            this.Text = "users";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.users_grid_closed);
            ((System.ComponentModel.ISupportInitialize)(this.users_grid_view)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView users_grid_view;
        private System.Windows.Forms.Button close;
    }
}