﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AuthenticationSystem
{
    public partial class authentication_form : Form
    {
        public authentication_form()
        {
            InitializeComponent();
            register_panel.Hide();
        }

        private void click_to_register_Click(object sender, EventArgs e)
        {
            register_panel.Show();
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            register_panel.Hide();
        }

        private void login_Click(object sender, EventArgs e)
        {

            string user_name = username.Text;
            string user_pass = password.Text;

            if (user_name == "" || user_pass == "")
            {
                MessageBox.Show("Both fields are required");
                return;
            }


            SqlConnection con = new SqlConnection(config.connectionString);
            con.Open();

            string command = "SELECT * FROM users where username = '" + user_name + "' and password = '" + user_pass + "'";
            SqlCommand cmd = new SqlCommand(command, con);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {

                this.Hide();
                users_grid userGrid = new users_grid();
                userGrid.Show();
                
            }
            else
            {
                MessageBox.Show("Invalid Username or password !. Please try again.");
            }

            con.Close();

        }

        private void authentication_form_closed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void register_Click(object sender, EventArgs e)
        {

            // All fields are required
            string firstName = first_name.Text;
            string lastName = last_name.Text;
            string userName = username_r.Text;
            string userPass = password_r.Text;
            string userPass2 = password2_r.Text;

            // Checking for empty column
            if (firstName == "" || lastName == "" || userName == "" || userPass == "" || userPass2 == "")
            {
                MessageBox.Show("All fields are required");
                return;
            }

            // Checking for passwords match
            if (userPass != userPass2)
            {
                MessageBox.Show("Password and Repeat Password Fields do not match");
                return;
            }


            SqlConnection con = new SqlConnection(config.connectionString);

            // checking for username duplication
            string checkSql = "SELECT username FROM users WHERE username = '{0}' ";
            checkSql = string.Format(checkSql, userName);

            SqlCommand checkCommand = new SqlCommand(checkSql, con);
            checkCommand.Connection.Open();
            SqlDataReader checkReader = checkCommand.ExecuteReader();
            if (checkReader.HasRows)
            {
                MessageBox.Show("Username already exists please try a different one.");
                checkCommand.Connection.Close();
                return;
            }
            checkCommand.Connection.Close();

            // All information is correct insert record in database
            string insertSql = "INSERT INTO users (username, password, first_name, last_name) VALUES ('{0}', '{1}', '{2}', '{3}') ";
            insertSql = string.Format(insertSql, userName, userPass, firstName, lastName);


            SqlCommand insertCommand = new SqlCommand(insertSql, con);
            insertCommand.Connection.Open();
            insertCommand.ExecuteNonQuery();
            insertCommand.Connection.Close();

            // new record inserted simply hide registeration form
            register_panel.Hide();
            MessageBox.Show("Please use your username and password to login.");
            return;

        }

    }
}
